import fs from "fs/promises";
import path from "path";

const themeDirectory = "node_modules/@chakra-ui/theme/dist/";
const filesToUpdate = ["chunk-SKQLGI36.mjs", "index.js", "semantic-tokens.js"];

async function updateThemeFiles() {
  for (const fileName of filesToUpdate) {
    const filePath = path.join(themeDirectory, fileName);

    try {
      const data = await fs.readFile(filePath, "utf8");

      const modifiedData = data.replace(
        /chakra-body-bg": { _light: ".*?", _dark: ".*?" },/,
        'chakra-body-bg": { _light: "#FCFCFC", _dark: "#1B1B1B" },',
      );

      await fs.writeFile(filePath, modifiedData, "utf8");
      console.log(`Chakra theme in ${fileName} updated successfully.`);
    } catch (err) {
      console.error(`Error updating theme file ${fileName}:`, err);
    }
  }
}

updateThemeFiles();
