// Lightmode menulist background color
export const lightbgColor = `#FCFCFC`;

// Darkmode menulist background color
export const darkbgColor = `#1B1B1B`;

// Lightmode border color
export const lightBorderColor = `#DDD`;

// Darkmode border color
export const darkBorderColor = `#808A8F50`;

// Lightmode Card background color
export const lightCardColor = `#d0dce74d`;

// Darkmode Card background color
export const darkCardColor = `#ffffff14`;

// Lightmode button background color
export const lightPostButtonColor = `#d0dce74d`;

// Darkmode button background color
export const darkPostButtonColor = `#ffffff14`;

// Lightmode menu color
export const lightMenuColor = `gray.800`;

// Darkmode menu color
export const darkMenuColor = `whiteAlpha.900`;

// Lightmode PCmenu color
export const lightPCMenubgColor = `#d0dce74d`;

// Darkmode PCmenu color
export const darkPCMenubgColor = `#38383880`;

// Lightmode menulist button color
export const lightMenubgColor = `#d0dce74d`;

// Darkmode menulist button color
export const darkMenubgColor = `#85858580`;

// Lightmode Navbar color
export const lightmainColor = `#D0DCE74D`;

// Darkmode Navbar color
export const darkmainColor = `#20202380`;

// Lightmode avatar background color
export const lightAvatarColor = `gray.200`;

// Darkmode avatar background color
export const darkAvatarColor = `transparent`;

// Lightmode avatarpage background color
export const lightfriendbgColor = `#FCFCFC`;

// Darkmode avatarpage background color
export const darkfriendbgColor = `#1B1B1B`;

// Lightmode comment userinfo background color
export const lightuserbgColor = `gray.100`;

// Darkmode comment userinfo background color
export const darkuserbgColor = `#1B1B1B`;

export const lightNavColor = `#FCFCFC`;

export const darkNavColor = `#1B1B1B`;
