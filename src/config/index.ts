export const domain = `blog.artcloud.top`;

export const language = `zh`;

export const author = `文艺`;

export const signAture = `兴趣使然`;

export const avatarUrl = `https://gitlab.com/uploads/-/system/user/avatar/15528178/avatar.png?width=400`;

export const siteTitle = `Art的博客`;

export const siteDescription = `欢迎进入我的学习日记`;

export const siteLogo = `/icon.png`;

export const darkLogo = `/darkicon.png`;

export const Licence = `CC BY-NC-SA 4.0`;

export const sourceUrl = `https://gitlab.com/wenyi401/memeblog-old`;

export const copyright = `Copyright ${new Date().getFullYear()} - ${author}`;

export const baseUrl = `https://${domain}`;
