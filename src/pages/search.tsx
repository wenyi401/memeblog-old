import React, { ReactNode, useState } from "react";
import { GetStaticProps } from "next";
import { getPostsFrontMatter, PartialFrontMatter } from "../lib/get-posts";
import Layout from "../components/layouts";
import {
  InputGroup,
  InputLeftAddon,
  Input,
  Tag,
  TagLeftIcon,
  TagLabel,
  Box,
} from "@chakra-ui/react";
import dynamic from "next/dynamic";
import { AddIcon } from "@chakra-ui/icons";
import { Link } from "@chakra-ui/next-js";
const PostsList = dynamic(() => import("../components/PostsList"));

type SearchProps = {
  Posts: PartialFrontMatter[];
};

const Search = ({ Posts }: SearchProps): ReactNode => {
  const [searchValue, setSearchValue] = useState("");

  const filteredBlogPosts = searchValue
    ? Posts.sort(
        (a, b) => Number(new Date(b.date)) - Number(new Date(a.date)),
      ).filter((frontMatter) =>
        frontMatter.title.toLowerCase().includes(searchValue.toLowerCase()),
      )
    : [];

  return (
    <Layout title="搜索">
      <Box p={2}>
        <InputGroup mt={16}>
          <InputLeftAddon>搜索</InputLeftAddon>
          <Input
            type="text"
            placeholder="帖子..."
            onChange={(e) => setSearchValue(e.target.value)}
            mb={5}
          />
        </InputGroup>
        {!filteredBlogPosts.length && searchValue && (
          <p>No posts found, try searching for something else.</p>
        )}
        <Link href="/tags">
          <Tag size="md" colorScheme="gray" mb={10}>
            <TagLeftIcon boxSize="12px" as={AddIcon} />
            <TagLabel>全部标签</TagLabel>
          </Tag>
        </Link>
        <PostsList posts={filteredBlogPosts} />
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = () => {
  const Posts = getPostsFrontMatter();
  return {
    props: {
      Posts,
    },
  };
};

export default Search;
