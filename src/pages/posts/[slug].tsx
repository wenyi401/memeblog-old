import { GetStaticPaths, GetStaticProps } from "next";
import { MDXRemote } from "next-mdx-remote";
import React, { ReactNode } from "react";
import { getAndSerializePost, getPosts, PostData } from "../../lib/get-posts";
import MDXComponents from "../../components/MDXComponents";
import Layout from "../../components/layouts";
import { motion } from "framer-motion";
import { 
  Heading,
  Box,
  useColorModeValue,
  Flex,
  Text,
  Avatar,
  Card,
  CardBody,
  Badge
} from "@chakra-ui/react";

import {
  author,
  avatarUrl,
  signAture,
} from "../../config/index";

import {
  lightAvatarColor,
  darkAvatarColor,
  lightCardColor,
  darkCardColor,
} from "../../config/sitestyles";

import "katex/dist/katex.min.css"

const postanimate = {
  hidden: { opacity: 0 },
  enter: { opacity: 1 },
  exit: { opacity: 0 },
};

const Article = ({ children }: { children: React.ReactNode }) => (
  <motion.article
    initial="hidden"
    animate="enter"
    exit="exit"
    variants={postanimate}
    transition={{ duration: 0.4, type: "easyInOut" }}
  >
    {children}
  </motion.article>
);

const ArticlePage = ({ mdxSource, frontMatter }: PostData): ReactNode => {
  const AvatarColor = useColorModeValue(lightAvatarColor, darkAvatarColor);
  const CardColor = useColorModeValue(lightCardColor, darkCardColor);
  return (
    <Layout
      title={`文章 - ${frontMatter.title}`}
      description={frontMatter.description}
      date={frontMatter.date}
    >
    <Box p={2} mt={12}>
      <Article>
          <Heading
            fontWeight="normal"
            fontSize="4xl"
            display={{ base: "none", md: "block" }}
          >
            {frontMatter.title}
          </Heading>
          <MDXRemote components={MDXComponents} {...mdxSource} />
        </Article>
      <Card bg={CardColor}>
        <CardBody>
          <Flex>
            <Avatar 
            src={avatarUrl}
            bg={AvatarColor}/>
            <Box ml='3'>
              <Text fontWeight='bold'>
                {author}
                <Badge ml='1' colorScheme='green'>
                  {frontMatter.date}
                </Badge>
              </Text>
              <Text fontSize='sm'>{signAture}</Text>
           </Box>
         </Flex>
       </CardBody>
       </Card>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { mdxSource, frontMatter } = await getAndSerializePost(
    params!.slug as string,
  );
  return {
    props: {
      mdxSource,
      frontMatter,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const posts = getPosts();

  return {
    paths: posts.map((p) => ({
      params: {
        slug: p.slug,
      },
    })),
    fallback: false,
  };
};

export default ArticlePage;
