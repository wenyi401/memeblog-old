import { GetStaticPaths, GetStaticProps } from "next";
import React, { ReactNode } from "react";
import { getAllTags } from "../lib/tags";
import { PartialFrontMatter } from "../lib/get-posts";
import PostsList from "../components/PostsList";
import Layout from "../components/layouts";
import { Text, Box } from "@chakra-ui/react";

type TagProps = {
  tag: string;
  posts: PartialFrontMatter[];
};

const TagPage = ({ tag, posts }: TagProps): ReactNode => {
  return (
    <Layout title={`标签  ${tag}`}>
      <Box p={2} mt={16}>
        <Text fontSize="lg" mb={5} display={{ base: "none", md: "block" }}>
          Tag &quot;{tag}&quot;
        </Text>
        <PostsList posts={posts} />
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const tag = params!.tag as string;
  const tagsWithPosts = getAllTags();
  return {
    props: {
      tag: tag,
      posts: tagsWithPosts.get(tag),
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const tagsWithPosts = getAllTags();
  return {
    paths: [...tagsWithPosts.keys()].map((t) => ({
      params: {
        tag: t,
      },
    })),
    fallback: false,
  };
};

export default TagPage;
