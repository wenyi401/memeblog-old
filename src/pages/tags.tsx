import { GetStaticProps } from "next";
import { getAllTags } from "../lib/tags";
import { PartialFrontMatter } from "../lib/get-posts";
import React, { ReactNode } from "react";
import { Link } from "@chakra-ui/next-js";
import Layout from "../components/layouts";
import { Tag, TagLabel, TagLeftIcon, Heading, Box } from "@chakra-ui/react";
import { MdTag } from "react-icons/md";

type TagsProps = {
  tags: [string, PartialFrontMatter[]][];
};

const TagsPage = ({ tags }: TagsProps): ReactNode => {
  return (
    <Layout title="所有标签">
      <Box mt={16}>
        <Heading ml={2} display={{ base: "none", md: "block" }}>
          Tags
        </Heading>
        {tags.map(([tag]) => {
          return (
            <Link href={`/${tag}`} key={tag}>
              <Tag size="lg" mr={3} ml={2} mt={4} mb={4}>
                <TagLeftIcon boxSize="12px" as={MdTag} />
                <TagLabel>{tag}</TagLabel>
              </Tag>
            </Link>
          );
        })}
      </Box>
    </Layout>
  );
};
export const getStaticProps: GetStaticProps = async () => {
  const tagsWithPosts = getAllTags(3);
  return {
    props: {
      tags: Array.from(tagsWithPosts),
    },
  };
};

export default TagsPage;
