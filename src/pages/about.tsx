import React, { ReactNode } from "react";
import Layout from "../components/layouts";
import {
  Box,
  Button,
  Text,
  Icon,
  useColorModeValue,
  Stack,
  useColorMode,
  Avatar,
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
} from "@chakra-ui/react";
import {
  LinkIcon,
  ViewIcon,
  WarningIcon,
  MoonIcon,
  SunIcon,
} from "@chakra-ui/icons";
import { Link } from "@chakra-ui/next-js";
import { GetStaticProps } from "next";
import friendsData from "../config/friends.json";
import { Licence, sourceUrl } from "../config/index";
import {
  lightAvatarColor,
  darkAvatarColor,
  lightfriendbgColor,
  darkfriendbgColor,
} from "../config/sitestyles";

type aboutProps = {
  friends: Array<{
    link: string;
    avatar: string;
    title: string;
    description: string;
    rss: string;
  }>;
};
const About = ({ friends }: aboutProps): ReactNode => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { colorMode, toggleColorMode } = useColorMode();
  const AvatarColor = useColorModeValue(lightAvatarColor, darkAvatarColor);
  const friendbgColor = useColorModeValue(
    lightfriendbgColor,
    darkfriendbgColor,
  );

  return (
    <Layout title="关于本站">
      <Box p={2} mt={16}>
        <Link href={sourceUrl}>
          <Button
            variant="ghost"
            width="full"
            mt={4}
            fontSize="lg"
            display="flex"
            justifyContent="flex-start"
            alignItems="center"
          >
            <Icon as={ViewIcon} mr={8} />
            <Text>源码</Text>
          </Button>
        </Link>
        <Button
          variant="ghost"
          width="full"
          onClick={onOpen}
          mt={4}
          fontSize="lg"
          display="flex"
          justifyContent="flex-start"
          alignItems="center"
        >
          <Icon as={LinkIcon} mr={8} />
          <Text>友链</Text>
        </Button>
        <Button
          variant="ghost"
          width="full"
          mt={4}
          fontSize="lg"
          display="flex"
          justifyContent="flex-start"
          alignItems="center"
        >
          <Icon as={WarningIcon} mr={8} />
          <Text>{Licence}</Text>
        </Button>
        <Drawer placement="left" onClose={onClose} isOpen={isOpen}>
          <DrawerOverlay />
          <DrawerContent bg={friendbgColor}>
            <DrawerHeader borderBottomWidth="1px">列表</DrawerHeader>
            <DrawerBody>
              {friends.map((friend, index) => (
                <Box key={index}>
                  <Box rounded="md" my={6}>
                    <Stack direction="row" alignItems="center" mb={5}>
                      <Avatar
                        src={friend.avatar}
                        name={friend.title}
                        bg={AvatarColor}
                      />
                      <Text fontSize="x-large">{friend.title}</Text>
                    </Stack>

                    {friend.description && (
                      <Text mb={2}>{friend.description}</Text>
                    )}
                    <Button ml={-4} variant="ghost">
                      <Link href={friend.link}>地址</Link>
                    </Button>
                    {friend.rss && (
                      <Button variant="ghost">
                        <Link href={friend.rss}>订阅</Link>
                      </Button>
                    )}
                  </Box>
                </Box>
              ))}
            </DrawerBody>
          </DrawerContent>
        </Drawer>
        <Button
          variant="ghost"
          width="full"
          onClick={toggleColorMode}
          mt={4}
          fontSize="lg"
          display="flex"
          justifyContent="flex-start"
          alignItems="center"
        >
          <Icon as={useColorModeValue(SunIcon, MoonIcon)} mr={8} />
          <Text> {colorMode === "light" ? "亮色主题" : "暗色主题"}</Text>
        </Button>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      friends: friendsData,
    },
  };
};

export default About;
