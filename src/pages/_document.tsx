import { Html, Head, Main, NextScript } from "next/document";
import theme from "../lib/theme";
import { ColorModeScript } from "@chakra-ui/react";
import { language } from "../config/index";

export default function Document() {
  return (
    <Html lang={`${language}`}>
      <Head />
      <body>
        <ColorModeScript initialColorMode={theme.config.initialColorMode} />
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
