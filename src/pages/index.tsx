import React, { ReactNode } from "react";
import Layout from "../components/layouts";

import { GetStaticProps } from "next";
import { getPostsFrontMatter } from "../lib/get-posts";
import { generateMainFeeds } from "../lib/feeds";
import {
  Box,
  Stat,
  StatLabel,
  StatNumber,
  StatHelpText,
  Button,
  Text,
  Icon,
  List,
  ListItem,
  Tab,
  Tabs,
  TabList,
  TabPanel,
  TabPanels,
  Tag,
  TagLabel,
  TagRightIcon,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverArrow,
  PopoverCloseButton,

} from "@chakra-ui/react";
import { Link } from "@chakra-ui/next-js";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { FaAt, FaBook, FaPhp, FaJava } from "react-icons/fa";
import { SiLua } from "react-icons/si";
import Image from "next/image";
const Home = (): ReactNode => {
  return (
    <Layout title="主页">
      <Box p={2}>
        <Box fontFamily="Menlo, Consolas, Monaco, Liberation Mono, Lucida Console, monospace">
          <Stat mt={16} mb={2}>
            <StatLabel>关于我</StatLabel>
            <StatNumber>文艺</StatNumber>
            <StatHelpText>(Art)</StatHelpText>
          </Stat>
          <Text my={4}>
            对编程兴趣使然的家伙，摆烂是常态，至今任是小白。
          </Text>
          <Link href="http://height.artcloud.top/" mr="2rem">
            <Button colorScheme="gray">
              光遇身高
              <ExternalLinkIcon mx="2px" />
            </Button>
          </Link>

          <Link href="/posts" mr="2rem">
            <Button colorScheme="gray">
                文章
               <Icon as={FaBook} w={4} h={4} mr={1} />
            </Button>
          </Link>
          <Popover>
            <PopoverTrigger>
              <Button colorScheme="gray">
                社交
                <Icon as={FaAt} w={4} h={4} mr={1} />
              </Button>
            </PopoverTrigger>
            <PopoverContent>
              <PopoverArrow/>
              <PopoverCloseButton/>
              <PopoverHeader>联系方式</PopoverHeader>
              <PopoverBody>github: wenyi401</PopoverBody>
            </PopoverContent>
          </Popover>
          <Box mt={5}>
            <Tabs>
              <TabList>
                <Tab>Languages</Tab> <Tab>Like</Tab>
              </TabList>
              <TabPanels>
                <TabPanel>
                  <List>
                    <ListItem>
                      <Tag size="md" my={3} mr={2}>
                        <TagLabel>Lua  </TagLabel>
                        <TagRightIcon as={SiLua} />
                      </Tag>
                    </ListItem>
                    <ListItem mt={3}>
                      <Tag size="md" my={3} mr={2}>
                        <TagLabel>Java  </TagLabel>
                        <TagRightIcon as={FaJava} />
                      </Tag>
                    </ListItem>
                    <ListItem mt={3}>
                      <Tag size="md" my={3} mr={2}>
                        <TagLabel>Php  </TagLabel>
                        <TagRightIcon as={FaPhp} />
                      </Tag>
                    </ListItem>
                  </List>
                </TabPanel>
                <TabPanel>
                  <Tag size="md" variant="outline" my={3}>
                    <TagLabel>泡面</TagLabel> 
                  </Tag>
                  <Image
                    src="/one-punch.jpg"
                    width={300}
                    height={300}
                    alt="logo"
                    loading="lazy"
                  />
                </TabPanel>
              </TabPanels>
            </Tabs>
          </Box>
        </Box>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = () => {
  generateMainFeeds();
  const allPosts = getPostsFrontMatter();
  return { props: { allPosts } };
};

export default Home;
