import React, { ReactNode } from "react";
import { GetStaticProps } from "next";
import { getPostsFrontMatter, PartialFrontMatter } from "../lib/get-posts";
import Layout from "../components/layouts";
import { Box, Grid } from "@chakra-ui/react";
import { TagsPostsList } from "../components/TagsPostsList";

type PostsProps = {
  Posts: PartialFrontMatter[];
};

const Posts = ({ Posts }: PostsProps): ReactNode => {
  return (
    <Layout title="文章">
      <Box mt={16}>
        <Grid gap={4}>
          <TagsPostsList posts={Posts} />
        </Grid>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = () => {
  const Posts = getPostsFrontMatter();
  return {
    props: {
      Posts,
    },
  };
};

export default Posts;
