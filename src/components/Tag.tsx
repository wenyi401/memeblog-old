import React, { ReactElement } from "react";
import { Link } from "@chakra-ui/next-js";
import { Tag, TagLabel, TagLeftIcon } from "@chakra-ui/react";
import { MdTag } from "react-icons/md";

type TagProps = {
  tag: string;
};

const CustomTag = ({ tag }: TagProps): ReactElement => {
  return (
    <Link href={"/" + tag}>
      <Tag size="sm" borderRadius="md" colorScheme="gray" mr={1}>
        <TagLeftIcon boxSize="12px" as={MdTag} />
        <TagLabel>{tag}</TagLabel>
      </Tag>
    </Link>
  );
};

export default CustomTag;
