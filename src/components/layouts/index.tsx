import Meta, { MetaProps } from "./Meta";
import React, { ReactElement, ReactNode } from "react";
import { Box, Container } from "@chakra-ui/react";
import Navbar from "../navbar";

type LayoutProps = MetaProps & {
  children: ReactNode;
};

const Layout = ({ children, ...meta }: LayoutProps): ReactElement => {
  return (
    <>
      <Meta {...meta} />
      <Box as="main" pb={8}>
        <Navbar>{meta.title}</Navbar>
        <Container maxW="container.lg" pt={1}>
          {children}
        </Container>
      </Box>
    </>
  );
};

export default Layout;
