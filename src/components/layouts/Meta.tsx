import Head from "next/head";
import { useRouter } from "next/router";
import { baseUrl, siteLogo, darkLogo } from "../../config/index";
import React, { ReactElement } from "react";
import { useColorMode, useColorModeValue } from "@chakra-ui/react";
import { lightmainColor, darkmainColor } from "../../config/sitestyles";

export type MetaProps = {
  title?: string;
  description?: string;
  image?: string;
  date?: string;
};

const Meta = (metaProps: MetaProps): ReactElement => {
  const { colorMode } = useColorMode();
  const lightColor = useColorModeValue(lightmainColor, darkmainColor);
  const themeColor = colorMode === "light" ? lightColor : darkmainColor;

  const router = useRouter();
  const meta = {
    ...metaProps,
  };
  return (
    <Head>
      <title>{meta.title}</title>
      <meta name="robots" content="follow, index" />
      <meta name="description" content={meta.description} />
      <meta property="og:url" content={`${baseUrl}${router.asPath}`} />
      <link rel="canonical" href={`${baseUrl}${router.asPath}`} />
      <meta property="og:site_name" content={meta.title} />
      <meta property="og:description" content={meta.description} />
      <meta property="og:title" content={meta.title} />
      <link
        rel="alternate"
        type="application/rss+xml"
        title=""
        href={`$(baseUrl}/feeds/atom.xml`}
      />
      {meta.date && (
        <meta property="article:published_time" content={meta.date} />
      )}
      <link rel="manifest" href="/manifest.json" />
      <link
        rel="apple-touch-icon"
        href={useColorModeValue(siteLogo, darkLogo)}
      />
      <meta name="theme-color" content={themeColor} />
    </Head>
  );
};

export default Meta;
