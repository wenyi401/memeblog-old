import { Textarea, Box, Button, useColorModeValue } from "@chakra-ui/react";
import { lightBorderColor, darkBorderColor } from "../../config/sitestyles";
import { useAuth0 } from "@auth0/auth0-react";

type CommentFormProps = {
  text: string;
  setText: Function;
  onSubmit: (_e: React.FormEvent) => Promise<void>;
};

export default function CommentForm({
  text,
  setText,
  onSubmit,
}: CommentFormProps) {
  const { isAuthenticated, logout, loginWithPopup } = useAuth0();

  const handleLogout = async () => {
    await logout();
    window.location.href = window.location.origin;
  };

  return (
    <form onSubmit={(e) => onSubmit(e)}>
      <Textarea
        borderColor={useColorModeValue(lightBorderColor, darkBorderColor)}
        placeholder={
          isAuthenticated
            ? `What are your thoughts?`
            : "Please login to leave a comment"
        }
        onChange={(e) => setText(e.target.value)}
        value={text}
        isDisabled={isAuthenticated ? false : true}
        variant="flushed"
        mt={8}
        mb={4}
      />

      <Box mb={10}>
        {isAuthenticated ? (
          <Box>
            <Button onClick={onSubmit} variant="outline" mr={4}>
              Send
            </Button>
            <Button variant="outline" onClick={handleLogout}>
              Log Out
            </Button>
          </Box>
        ) : (
          <Button onClick={() => loginWithPopup()}>Log In</Button>
        )}
      </Box>
    </form>
  );
}
