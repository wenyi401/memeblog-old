import CommentForm from "./form";
import CommentList from "./list";
import useComments from "../../hooks/useComment";
import { Box, useColorModeValue } from "@chakra-ui/react";
import { lightBorderColor, darkBorderColor } from "../../config/sitestyles";

export default function Comment() {
  const { text, setText, comments, onSubmit, onDelete } = useComments();
  const commentBorderColor = useColorModeValue(
    lightBorderColor,
    darkBorderColor,
  );

  return (
    <Box
      borderColor={commentBorderColor}
      borderWidth="1px"
      rounded="md"
      p={4}
      mx={-4}
      mt={20}
    >
      <CommentForm onSubmit={onSubmit} text={text} setText={setText} />

      <CommentList comments={comments} onDelete={onDelete} />
    </Box>
  );
}
