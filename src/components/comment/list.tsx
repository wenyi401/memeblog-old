import type { Comment } from "../../interfaces";
import distanceToNow from "../../lib/dateRelative";
import { useAuth0 } from "@auth0/auth0-react";
import Image from "next/image";
import {
  Box,
  IconButton,
  Badge,
  Stack,
  Text,
  Flex,
  useColorModeValue,
} from "@chakra-ui/react";
import { SmallCloseIcon } from "@chakra-ui/icons";
import {
  lightBorderColor,
  darkBorderColor,
  lightuserbgColor,
  darkuserbgColor,
} from "../../config/sitestyles";

type CommentListProps = {
  comments?: Comment[];
  onDelete: (_comment: Comment) => Promise<void>;
};

export default function CommentList({ comments, onDelete }: CommentListProps) {
  const { user } = useAuth0();
  const borderbgColor = useColorModeValue(lightBorderColor, darkBorderColor);
  const userbgColor = useColorModeValue(lightuserbgColor, darkuserbgColor);
  return (
    <Box>
      {comments &&
        comments.map((comment) => {
          const isAuthor = user && user.sub === comment.user.sub;
          const isAdmin =
            user && user.email === process.env.NEXT_PUBLIC_AUTH0_ADMIN_EMAIL;

          return (
            <Box
              key={comment.created_at}
              borderColor={borderbgColor}
              borderWidth="1px"
              rounded="md"
              p={2}
              my={6}
            >
              <Box mx={-2} mt={-2} mb={4} p={2} rounded="md" bg={userbgColor}>
                <Stack direction="row" alignItems="center">
                  <Image
                    src={comment.user.picture}
                    alt={comment.user.name}
                    width={40}
                    height={40}
                    style={{ borderRadius: "50%", overflow: "hidden" }}
                  />

                  <Text>{comment.user.name}</Text>
                </Stack>
                <Badge fontSize="sm" mt={4} variant=",ghost">
                  {distanceToNow(comment.created_at)}
                </Badge>
                {(isAdmin || isAuthor) && (
                  <Flex justifyContent="flex-end">
                    <IconButton
                      isRound
                      size="sm"
                      mt={-10}
                      onClick={() => onDelete(comment)}
                      aria-label="Delete comment"
                      variant="ghost"
                      icon={<SmallCloseIcon />}
                    />
                  </Flex>
                )}
              </Box>

              <Box>{comment.text}</Box>
            </Box>
          );
        })}
    </Box>
  );
}
