import { PartialFrontMatter } from "../lib/get-posts";
import React, { ReactElement, useState } from "react";
import {
  Box,
  chakra,
  shouldForwardProp,
  Tag,
  TagLabel,
  TagLeftIcon,
  GridItem,
  Text,
  Center,
  Button,
  IconButton,
  useColorModeValue,
} from "@chakra-ui/react";
import { Link } from "@chakra-ui/next-js";
import { motion } from "framer-motion";
import { MdTag } from "react-icons/md";
import {
  lightBorderColor,
  darkBorderColor,
  lightPostButtonColor,
  darkPostButtonColor,
} from "../config/sitestyles";
import Image from "next/image";
import { ChevronLeftIcon, ChevronRightIcon } from "@chakra-ui/icons";
const StyledDiv = chakra(motion.div, {
  shouldForwardProp: (prop) => {
    return shouldForwardProp(prop) || prop === "transition";
  },
});

const Section = ({ children }: { children: React.ReactNode }) => (
  <StyledDiv
    initial={{ y: 10, opacity: 0 }}
    animate={{ y: 0, opacity: 1 }}
    mb={2}
  >
    {children}
  </StyledDiv>
);

const Excerpt = ({ children }: { children: React.ReactNode }) => (
  <Text fontSize="sm">{children}</Text>
);

type PostsListProps = {
  posts: PartialFrontMatter[];
};

type PostProps = {
  frontMatter: PartialFrontMatter;
};

function Post({ frontMatter }: PostProps): ReactElement {
  const brightnessInDarkMode = useColorModeValue("none", "brightness(75%)");
  return (
    <Section>
      <GridItem>
        <Box
          borderWidth="1px"
          rounded="md"
          p={3}
          borderColor={useColorModeValue(lightBorderColor, darkBorderColor)}
        >
          <Text fontSize="sm" mb={1}>
            {new Date(frontMatter.date).toDateString()}
            {frontMatter.tags?.map((t) => (
              <Link href={`/${t}`} key={t}>
                <Tag size="sm" bg="transparent" ml={2}>
                  <TagLeftIcon boxSize="12px" as={MdTag} />
                  <TagLabel>{t}</TagLabel>
                </Tag>
              </Link>
            ))}
          </Text>
          <Text fontSize="x-large" fontWeight="bold" mb={1}>
            {frontMatter.title}
          </Text>
          <Excerpt>{frontMatter.description}</Excerpt>
          {frontMatter.picture ? (
            <Image
              alt={frontMatter.picture}
              src={frontMatter.picture}
              style={{
                width: "100%",
                height: "auto",
                margin: "10px 0",
                objectFit: "contain",
                borderRadius: "10px",
                maxHeight: "25rem",
                filter: brightnessInDarkMode,
              }}
              width={200}
              height={80}
              sizes="100vw"
            />
          ) : null}

          <Link href={`/posts/${frontMatter.slug}`}>
            <Center
              mt={2}
              rounded="md"
              bg={useColorModeValue(lightPostButtonColor, darkPostButtonColor)}
              p={2}
              fontSize="sm"
            >
              阅读
            </Center>
          </Link>
        </Box>
      </GridItem>
    </Section>
  );
}
export const TagsPostsList = ({ posts }: PostsListProps): ReactElement => {
  const postsPerPage = 10;
  const [currentPage, setCurrentPage] = useState(1);

  const totalPages = Math.ceil(posts.length / postsPerPage);

  const handlePageChange = (pageNumber: number) => {
    if (pageNumber >= 1 && pageNumber <= totalPages) {
      setCurrentPage(pageNumber);
    }
  };

  const renderPageNumbers = () => {
    const pageNumbers = [];
    for (let i = 1; i <= totalPages; i++) {
      pageNumbers.push(i);
    }
    return pageNumbers.map((pageNumber) => (
      <Button
        key={pageNumber}
        onClick={() => handlePageChange(pageNumber)}
        variant={pageNumber === currentPage ? "solid" : "ghost"}
        rounded={pageNumber === currentPage ? "full" : "null"}
        style={{
          margin: "0.2rem",
          fontWeight: pageNumber === currentPage ? "bold" : "normal",
        }}
      >
        {pageNumber}
      </Button>
    ));
  };

  const renderPagination = () => {
    return (
      <Box>
        <IconButton
          isRound={true}
          aria-label="Done"
          fontSize="20px"
          onClick={() => handlePageChange(currentPage - 1)}
          icon={<ChevronLeftIcon />}
          variant="ghost"
          mr={1}
        />

        {renderPageNumbers()}
        <IconButton
          isRound={true}
          aria-label="Done"
          fontSize="20px"
          variant="ghost"
          onClick={() => handlePageChange(currentPage + 1)}
          icon={<ChevronRightIcon />}
          ml={1}
        />
      </Box>
    );
  };

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

  return (
    <>
      {currentPosts.map((frontMatter) => {
        return <Post key={frontMatter.title} frontMatter={frontMatter} />;
      })}
      {renderPagination()}
    </>
  );
};
