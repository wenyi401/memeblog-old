import { PartialFrontMatter } from "../lib/get-posts";
import React, { ReactElement } from "react";
import {
  Box,
  chakra,
  shouldForwardProp,
  Tag,
  TagLabel,
  TagLeftIcon,
  Text,
  Center,
  useColorModeValue,
} from "@chakra-ui/react";
import { Link } from "@chakra-ui/next-js";
import { motion } from "framer-motion";
import { MdTag } from "react-icons/md";
import {
  lightBorderColor,
  darkBorderColor,
  lightPostButtonColor,
  darkPostButtonColor,
} from "../config/sitestyles";
import Image from "next/image";

const StyledDiv = chakra(motion.div, {
  shouldForwardProp: (prop) => {
    return shouldForwardProp(prop) || prop === "transition";
  },
});

const Section = ({ children }: { children: React.ReactNode }) => (
  <StyledDiv
    initial={{ y: 10, opacity: 0 }}
    animate={{ y: 0, opacity: 1 }}
    mb={2}
  >
    {children}
  </StyledDiv>
);

const Excerpt = ({ children }: { children: React.ReactNode }) => (
  <Text fontSize="sm">{children}</Text>
);

type PostsListProps = {
  posts: PartialFrontMatter[];
};

type PostProps = {
  frontMatter: PartialFrontMatter;
};

function Post({ frontMatter }: PostProps): ReactElement {
  const brightnessInDarkMode = useColorModeValue("none", "brightness(75%)");
  return (
    <Section>
      <Box
        borderWidth="1px"
        rounded="md"
        p={3}
        borderColor={useColorModeValue(lightBorderColor, darkBorderColor)}
      >
        <Text fontSize="sm" mb={1}>
          {new Date(frontMatter.date).toDateString()}
          {frontMatter.tags?.map((t) => (
            <Link href={`/${t}`} key={t}>
              <Tag size="sm" bg="transparent" ml={2}>
                <TagLeftIcon boxSize="12px" as={MdTag} />
                <TagLabel>{t}</TagLabel>
              </Tag>
            </Link>
          ))}
        </Text>
        <Text fontSize="x-large" fontWeight="bold" mb={1}>
          {frontMatter.title}
        </Text>
        <Excerpt>{frontMatter.description}</Excerpt>
        {frontMatter.picture ? (
          <Image
            alt={frontMatter.picture}
            src={frontMatter.picture}
            style={{
              width: "100%",
              height: "auto",
              margin: "10px 0",
              objectFit: "contain",
              borderRadius: "10px",
              maxHeight: "25rem",
              filter: brightnessInDarkMode,
            }}
            width={200}
            height={80}
            sizes="100vw"
          />
        ) : null}

        <Link href={`/posts/${frontMatter.slug}`}>
          <Center
            mt={2}
            rounded="md"
            bg={useColorModeValue(lightPostButtonColor, darkPostButtonColor)}
            p={2}
            fontSize="sm"
          >
            阅读
          </Center>
        </Link>
      </Box>
    </Section>
  );
}
const PostsList = ({ posts }: PostsListProps): ReactElement => {
  return (
    <>
      {posts.map((frontMatter) => {
        return <Post key={frontMatter.title} frontMatter={frontMatter} />;
      })}
    </>
  );
};

export default PostsList;
