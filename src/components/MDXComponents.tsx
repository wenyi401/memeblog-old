import React from "react";
import {
  chakra,
  Divider,
  Heading,
  OrderedList,
  Text,
  UnorderedList,
  Code,
  Table,
  Thead,
  Tbody,
  Tr,
  Box,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Th,
  Step,
  StepDescription,
  StepIcon,
  StepIndicator,
  StepNumber,
  StepSeparator,
  StepStatus,
  StepTitle,
  Stepper,
  Td,
  IconButton,
  LinkProps,
  ListItem,
  useColorMode,
  useClipboard,
  useColorModeValue,
} from "@chakra-ui/react";
import Image from "next/image";
import { lightBorderColor, darkBorderColor } from "../config/sitestyles";
import { ChakraProps, HeadingProps, HTMLChakraProps } from "@chakra-ui/react";
import { ImageProps } from "next/image";
import { Highlight, themes } from "prism-react-renderer";
import { Link } from "@chakra-ui/next-js";
import { CopyIcon, CheckIcon } from "@chakra-ui/icons"

type CodeHighlightProps = {
  children: string;
  className: string;
};

type CustomImageProps = Omit<ImageProps, "src"> & {
  src: string;
  alt: string;
};

const CustomLink: React.FC<LinkProps> = ({ href, ...rest }) => {
  return <Link href={href!} textDecoration="underline" {...rest} />;
};

const CustomImage: React.FC<CustomImageProps> = ({ src, alt, ...rest }) => {
  return (
    <Image
      width={300}
      height={300}
      sizes="(max-width: 768px) 60vw"
      src={src}
      alt={alt}
      {...rest}
    />
  );
};

const Quote: React.FC<ChakraProps> = (props) => {
  return (
    <chakra.blockquote
      mt={3}
      pl="20px"
      marginInlineStart="20px"
      borderLeftWidth="2px"
      borderLeftColor={useColorModeValue(lightBorderColor, darkBorderColor)}
      bg="transparent"
      {...props}
    />
  );
};

const Hr: React.FC = () => {
  const { colorMode } = useColorMode();
  const borderColor = {
    light: lightBorderColor,
    dark: darkBorderColor,
  };

  return <Divider borderColor={borderColor[colorMode]} my={4} w="100%" />;
};

const CodeHighlight = ({
  children: codeString,
  className: language,
}: CodeHighlightProps) => {
  const theme = useColorModeValue(themes.github, themes.vsDark);
  const lineNumberColor = useColorModeValue("blackAlpha.500", "whiteAlpha.500");
  const preBackground = useColorModeValue("gray.100", "gray.900");
  const bdColor = useColorModeValue(lightBorderColor, darkBorderColor);
  const { onCopy, hasCopied } = useClipboard(codeString);

  if (language && language.startsWith("language-")) {
    language = language.replace("language-", "");

    const showLineNumbers = !["shell", "text"].includes(language);

    return (
      <Highlight code={codeString} theme={theme} language={language}>
        {({ className, style, tokens, getLineProps, getTokenProps }) => {
          return (
            <div data-language={className} style={{ position: "relative" }}>
                         <Box><IconButton
                         aria-label='copy code'
                         isRound={true}
                         icon={hasCopied? <CheckIcon />: <CopyIcon />}
                         variant="outline"
                onClick={onCopy}
                style={{
                  position: "absolute",
                  top: "0.5rem",
                  right: "0.5rem",
                  cursor: "pointer",
                }}
             / >
              </Box>
              <chakra.pre
                className={className}
                style={{ ...style, backgroundColor: preBackground }}
                overflowX="auto"
                borderWidth="1px"
                borderColor={bdColor}
                rounded="md"
                p={4}
                my={4}
                mx={-2}
              >
                {tokens.map((line, i) => {
                  const lineProps = getLineProps({ line, key: i });
                  return (
                    <chakra.div {...lineProps} display="table-row" key={i}>
                      {showLineNumbers && (
                        <chakra.span
                          w={8}
                          display="table-cell"
                          textAlign="right"
                          userSelect="none"
                          color={lineNumberColor}
                          pr={3}
                        >
                          {i + 1}
                        </chakra.span>
                      )}
                      {line.map((token, key) => {
                        return (
                          <chakra.span
                            {...getTokenProps({ token, key })}
                            key={`${i}.${key}`}
                          />
                        );
                      })}
                    </chakra.div>
                  );
                })}
              </chakra.pre>
            </div>
          );
        }}
      </Highlight>
    );
  } else {
    return <Code>{codeString}</Code>;
  }
};

const MDXComponents = {
  code: CodeHighlight as any,
  h1: (props: HeadingProps) => (
    <Heading
      as="h1"
      fontSize="3xl"
      pb={0.5}
      fontWeight="normal"
      mb={3}
      mt={3}
      {...props}
    />
  ),
  h2: (props: HeadingProps) => (
    <Heading
      as="h2"
      fontSize="2xl"
      pb={0.5}
      fontWeight="normal"
      mb={3}
      mt={3}
      {...props}
    />
  ),
  h3: (props: HeadingProps) => (
    <Heading
      as="h3"
      fontSize="xl"
      pb={0.5}
      fontWeight="normal"
      mb={3}
      mt={3}
      {...props}
    />
  ),
  h4: (props: HeadingProps) => (
    <Heading
      as="h4"
      fontSize="lg"
      {...props}
      pb={0.5}
      fontWeight="normal"
      mb={2}
      mt={2}
    />
  ),
  hr: Hr,
  p: (props: HTMLChakraProps<"p">) => (
    <Text mt={2} mb={4} lineHeight={1.5} letterSpacing={1} {...props} />
  ),
  ul: (props: HTMLChakraProps<"ul">) => (
    <UnorderedList as="ul" pt={2} pl={4} mt={4} mb={4} {...props} />
  ),
  ol: (props: HTMLChakraProps<"ol">) => (
    <OrderedList as="ol" pt={2} pl={6} mt={4} mb={4} {...props} />
  ),
  li: (props: HTMLChakraProps<"li">) => <ListItem as="li" {...props} />,
  blockquote: Quote,
  img: CustomImage as React.ComponentType<React.HTMLProps<HTMLImageElement>>,
  table: (props: HTMLChakraProps<"table">) => (
    <Table mt={4} variant="striped" {...props} />
  ),
  thead: Thead,
  tbody: Tbody,
  tr: Tr,
  th: Th,
  td: Td,

  Stepwrap: ({
    children,
    step,
    ...rest
  }: {
    children: React.ReactNode;
    step: number;
    [key: string]: any;
  }) => {
    return (
      <Stepper index={step} my={6} colorScheme="gray" size="sm" {...rest}>
        {children}
      </Stepper>
    );
  },
  Step: ({ children }: { children: React.ReactNode }) => {
    return (
      <Step>
        <StepIndicator>
          <StepStatus
            complete={<StepIcon />}
            incomplete={<StepNumber />}
            active={<StepNumber />}
          />
        </StepIndicator>

        <Box flexShrink="0">{children}</Box>
        <StepSeparator />
      </Step>
    );
  },
  Stepdesc: StepDescription,
  Steptitle: StepTitle,
  Expan: ({ children }: { children: React.ReactNode }) => {
    return (
      <Accordion my={6} allowMultiple>
        <AccordionItem>{children}</AccordionItem>
      </Accordion>
    );
  },
  Expantitle: ({ children }: { children: React.ReactNode }) => {
    return (
      <AccordionButton>
        <Box as="span" flex="1" textAlign="left" mx={-3}>
          {children}
        </Box>
        <AccordionIcon />
      </AccordionButton>
    );
  },
  Expancontent: ({ children }: { children: React.ReactNode }) => {
    return (
      <AccordionPanel pb={4} mx={-3}>
        {children}
      </AccordionPanel>
    );
  },
  a: CustomLink,
};

export default MDXComponents;
