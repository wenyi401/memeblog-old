import { Link } from "@chakra-ui/next-js";
import {
  Container,
  Box,
  Menu,
  MenuItem,
  MenuList,
  MenuButton,
  IconButton,
  Text,
  useBreakpointValue,
  Flex,
  useColorModeValue,
} from "@chakra-ui/react";
import { HamburgerIcon, SearchIcon, ChevronLeftIcon } from "@chakra-ui/icons";
import React from "react";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import {
  lightMenuColor,
  darkMenuColor,
  lightMenubgColor,
  darkMenubgColor,
  lightPCMenubgColor,
  darkPCMenubgColor,
  lightNavColor,
  darkNavColor,
  lightmainColor,
  darkmainColor,
  lightbgColor,
  darkbgColor,
} from "../config/sitestyles";
import MobileLinks from "../config/mobilelinks.json";
import PCLinks from "../config/pclinks.json";

type LinksProps = {
  children: React.ReactNode;
};
type mblinksProps = {
  mblinks: Array<{
    link: string;
    title: string;
  }>;
};
type pclinksProps = {
  pclinks: Array<{
    link: string;
    title: string;
  }>;
};

const LinkItem = ({ pclinks }: pclinksProps) => {
  const router = useRouter();
  const path = router.asPath;
  const LinkColor = useColorModeValue(lightMenuColor, darkMenuColor);
  const bgColor = useColorModeValue(lightPCMenubgColor, darkPCMenubgColor);

  return (
    <div>
      {pclinks.map((pclink, index) => {
        const active = path === pclink.link;
        return (
          <Link
            key={index}
            href={pclink.link}
            scroll={false}
            p={2}
            color={LinkColor}
            bg={active ? bgColor : undefined}
            borderRadius="md"
          >
            {pclink.title}
          </Link>
        );
      })}
    </div>
  );
};

const MenuLinks = ({ mblinks }: mblinksProps) => {
  const router = useRouter();
  const path = router.asPath;
  const LinkColor = useColorModeValue(lightMenuColor, darkMenuColor);
  const bgColor = useColorModeValue(lightMenubgColor, darkMenubgColor);
  return (
    <div>
      {mblinks.map((mblink, index) => {
        const active = path === mblink.link;

        return (
          <Link key={index} href={mblink.link}>
            <MenuItem color={LinkColor} bg={active ? bgColor : "transparent"}>
              {mblink.title}
            </MenuItem>
          </Link>
        );
      })}
    </div>
  );
};
const Navbar = ({ children }: LinksProps) => {
  const router = useRouter();
  const isSearchPage = router.asPath === "/search";
  const isAboutPage = router.asPath === "/about";
  const isMobileView = useBreakpointValue({ base: true, md: false });
  const isComputerView = useBreakpointValue({ base: false, md: true });

  const [scrollPosition, setScrollPosition] = useState(0);
  const [interpolatedColor, setInterpolatedColor] = useState("");

  useEffect(() => {
    const handleScroll = () => {
      const currentPosition = window.scrollY;
      setScrollPosition(currentPosition);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const initialColor = useColorModeValue(lightNavColor, darkNavColor);
  const targetColor = useColorModeValue(lightmainColor, darkmainColor);

  useEffect(() => {
    const calculateInterpolatedColor = (
      startColor: string,
      endColor: string,
      progress: number,
    ) => {
      const startR = parseInt(startColor.slice(1, 3), 16);
      const startG = parseInt(startColor.slice(3, 5), 16);
      const startB = parseInt(startColor.slice(5, 7), 16);

      const endR = parseInt(endColor.slice(1, 3), 16);
      const endG = parseInt(endColor.slice(3, 5), 16);
      const endB = parseInt(endColor.slice(5, 7), 16);

      const interpolatedR = Math.floor(startR + (endR - startR) * progress);
      const interpolatedG = Math.floor(startG + (endG - startG) * progress);
      const interpolatedB = Math.floor(startB + (endB - startB) * progress);

      return `rgb(${interpolatedR}, ${interpolatedG}, ${interpolatedB})`;
    };

    const progress = Math.min(1, scrollPosition / 50);

    const interpolatedColor = calculateInterpolatedColor(
      initialColor,
      targetColor,
      progress,
    );

    setInterpolatedColor(interpolatedColor);
  }, [scrollPosition, initialColor, targetColor]);

  const MenuListbgColor = useColorModeValue(lightbgColor, darkbgColor);
  return (
    <Box
      position="fixed"
      as="nav"
      w="100%"
      bg={scrollPosition > 0 ? interpolatedColor : undefined}
      transition="background 0.3s"
      zIndex={1}
    >
      <Container p={2} maxW="container.lg">
        <Flex alignItems="center">
          {isMobileView && !isSearchPage && !isAboutPage && (
            <Menu autoSelect={false}>
              <MenuButton
                as={IconButton}
                aria-label="Options"
                icon={<HamburgerIcon />}
                variant="ghost"
                aria-controls="none"
                _focus={{ bg: "transparent" }}
              />
              <MenuList bg={MenuListbgColor}>
                <MenuLinks mblinks={MobileLinks} />
              </MenuList>
            </Menu>
          )}

          {(isSearchPage || isAboutPage) && isMobileView && (
            <Link href="/">
              <IconButton
                aria-label="Back"
                icon={<ChevronLeftIcon />}
                variant="ghost"
                onClick={() => router.back()}
              />
            </Link>
          )}
          <Text
            fontWeight="bold"
            fontSize="lg"
            ml={2}
            display={isMobileView ? "block" : "none"}
          >
            {children}
          </Text>

          {isMobileView && !isSearchPage && (
            <Box flex={1} textAlign="right">
              <Link href="/search">
                <IconButton
                  aria-label="Search database"
                  icon={<SearchIcon />}
                  variant="ghost"
                />
              </Link>
            </Box>
          )}
          {isComputerView && (
            <Box mt={4}>
              <LinkItem pclinks={PCLinks} />
            </Box>
          )}
        </Flex>
      </Container>
    </Box>
  );
};

export default Navbar;
